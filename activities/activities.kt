import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.mainActivity.*
import kotlinx.android.synthetic.main.splashActivity.*
import kotlinx.android.synthetic.main.infoActivity.*

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }, 3000)
    }
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_next.setOnClickListener {
            val intent = Intent(this@MainActivity, InfoActivity::class.java)
            intent.putExtra("firstName", et_first_name.text.toString())
            intent.putExtra("lastName", et_last_name.text.toString())
            intent.putExtra("middleName", et_middle_name.text.toString())
            intent.putExtra("age", et_age.text.toString())
            intent.putExtra("hobby", et_hobby.text.toString())
            startActivity(intent)
        }
    }
}

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val firstName = intent.getStringExtra("firstName")
        val lastName = intent.getStringExtra("lastName")
        val middleName = intent.getStringExtra("middleName")
        val age = intent.getStringExtra("age")?.toIntOrNull()
        val hobby = intent.getStringExtra("hobby")
        if (firstName != null && lastName != null && middleName != null && age != null && hobby != null) {
            val infoText = when (age) {
                in 0..12 -> "$firstName $middleName $lastName is a child who likes $hobby."
                in 13..19 -> "$firstName $middleName $lastName is a teenager who likes $hobby."
                in 20..59 -> "$firstName $middleName $lastName is an adult who likes $hobby."
                else -> "$firstName $middleName $lastName is a senior citizen who likes $hobby."
            }
            tv_info.text = infoText
        }
    }
}
