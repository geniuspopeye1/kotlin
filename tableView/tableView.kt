import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.screen.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            val randomIndex = Random().nextInt(4) + 1
            val imageResourceId = when (randomIndex) {
                1 -> R.drawable.image1
                2 -> R.drawable.image2
                3 -> R.drawable.image3
                4 -> R.drawable.image4
                else -> R.drawable.image1
            }
            imageView.setImageResource(imageResourceId)
        }
    }
}
