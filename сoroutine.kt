//First task
import kotlinx.coroutines.*

fun main() {
    GlobalScope.launch {
        delay(1000)
        println("World")
    }
    println("Hello, ")
    Thread.sleep(2000)
    println("main: I'm tired of waiting!")
}

//Second task 
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun getNum1(): Int {
    delay(1000)
    return 10
}

suspend fun getNum2(): Int {
    delay(1500)
    return 20
}

fun main() {
    val time = measureTimeMillis {
        val num1 = runBlocking { getNum1() }
        val num2 = runBlocking { getNum2() }
        val sum = num1 + num2
        println("Sequential result: $sum")
    }
    println("Sequential time: $time ms")

    val deferredNum1 = GlobalScope.async { getNum1() }
    val deferredNum2 = GlobalScope.async { getNum2() }

    val asyncTime = measureTimeMillis {
        val sum = runBlocking {
            deferredNum1.await() + deferredNum2.await()
        }
        println("Async result: $sum")
    }
    println("Async time: $asyncTime ms")

    for (i in 0..2) {
        println("I'm sleeping $i...")
        Thread.sleep(1000)
    }
    println("main: I'm tired of waiting!")
    runBlocking {
        delay(1000)
        println("I'm running finally")
    }
    println("main: Now I can quit.")
}